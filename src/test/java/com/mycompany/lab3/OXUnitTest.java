package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author HP
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    @Test
    public void testCheckWin_X_Row_1_output_true() {
        char [][]table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char Player = 'X';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
    
    @Test
    public void testCheckWin_O_Row_1_output_true() {
        char [][]table = {{'O','O','O'},{'-','X','-'},{'X','-','O'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
     @Test
    public void testCheckWin_X_Row_1_output_false() {
        char [][]table = {{'X','O','X'},{'X','-','-'},{'-','-','-'}};
        char Player = 'X';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWin_X_Row_2_output_true() {
        char [][]table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char Player = 'X';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
    
    @Test
    public void testCheckWin_O_Row_2_output_true() {
        char [][]table = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
     @Test
    public void testCheckWin_O_Row_2_output_false() {
        char [][]table = {{'O','X','-'},{'X','O','O'},{'-','-','-'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(false, result);
 }
    
    @Test
    public void testCheckWin_X_Row_3_output_true() {
        char [][]table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char Player = 'X';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
    
    @Test
    public void testCheckWin_O_Row_3_output_true() {
        char [][]table = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
    
    @Test
    public void testCheckWin_O_Row_3_output_False() {
        char [][]table = {{'-','-','-'},{'-','-','-'},{'-','O','O'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(false, result);
 }
    
    @Test
    public void testCheckWin_O_Col_1_output_true() {
        char [][]table = {{'O','-','-'},{'O','-','-'},{'O','-','-'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
    
    @Test
    public void testCheckWin_X_Col_1_output_false() {
        char [][]table = {{'X','-','-'},{'O','O','-'},{'X','-','-'}};
        char Player = 'X';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(false, result);
 }
    
    @Test
    public void testCheckWin_O_Col_2_output_true() {
        char [][]table = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
    
    @Test
    public void testCheckWin_X_Col_2_output_false() {
        char [][]table = {{'O','X','-'},{'O','X','-'},{'O','O','-'}};
        char Player = 'X';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(false, result);
 }
    
    @Test
    public void testCheckWin_O_Col_3_output_true() {
        char [][]table = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
    
    @Test
    public void testCheckWin_O_Col_3_output_false() {
        char [][]table = {{'-','-','-'},{'-','-','O'},{'-','-','O'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(false, result);
 }
    
    @Test
    public void testCheckWin_X_Diagonal_left_output_true() {
        char [][]table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char Player = 'X';
        boolean result = Lab3.checkWin_Diagonal_left(table,Player);
        assertEquals(true, result);
    }
        
    @Test
    public void testCheckWin_O_Diagonal_left_output_false() {
        char [][]table = {{'-','-','-'},{'-','O','-'},{'-','-','O'}};
        char Player = 'O';
        boolean result = Lab3.checkWin_Diagonal_left(table,Player);
        assertEquals(false, result);
 }
    @Test
    public void testCheckWin_X_Diagonal_rigth_output_true() {
        char [][]table = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        char Player = 'X';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(true, result);
 }
    @Test
    public void testCheckWin_Diagonal_rigth_output_false() {
        char [][]table = {{'-','-','O'},{'-','O','-'},{'-','-','-'}};
        char Player = 'O';
        boolean result = Lab3.checkWin(table,Player);
        assertEquals(false, result);
 }

    @Test
    public void testCheckDraw_X_output_true() {
        char [][]table = {{'O','X','X'},{'X','O','O'},{'O','X','X'}};
        char Player = 'X';
        boolean result = Lab3.checkDraw(table,Player);
        assertEquals(true, result);
 }
    
    @Test
    public void testCheckDraw_O_output_true() {
        char [][]table = {{'X','O','X'},{'X','O','O'},{'O','X','X'}};
        char Player = 'O';
        boolean result = Lab3.checkDraw(table,Player);
        assertEquals(true, result);
 }
    
 
    
    
    
     
        
 }
    
    
    


  
